##################################
# fichier departement-de-chimie--melange-explosif-validation.py
# nom de l'exercice :  Département de chimie : mélange explosif
# url : http://www.france-ioi.org/algo/task.php?idChapter=649&idTask=0&sTab=task&iOrder=7
# type : validation
#
# Nom du chapitre : 
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nbmes=int(input())
tmin=int(input())
tmax=int(input())
t=int(input())
nb=0
while (t>=tmin and tmax>=t)and nb<nbmes:
   nb+=1
   print("Rien à signaler")
   if nb<nbmes:
      t=int(input())
if t<tmin or t>tmax:
   print("Alerte !!")
