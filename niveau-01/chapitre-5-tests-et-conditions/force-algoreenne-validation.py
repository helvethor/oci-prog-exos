##################################
# fichier force-algoreenne-validation.py
# nom de l'exercice :  Force algoréenne
# url : http://www.france-ioi.org/algo/task.php?idChapter=646&idTask=0&sTab=task&iOrder=1
# type : validation
#
# Nom du chapitre : 
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

equipe1=0
equipe2=0
for loop in range(int(input())):
   equipe1=equipe1+int(input())
   equipe2=equipe2+int(input())
if equipe1>equipe2:
   print("L'équipe 1 a un avantage")
else:
   print("L'équipe 2 a un avantage")
print("Poids total pour l'équipe 1 : ",end="")
print(equipe1)
print("Poids total pour l'équipe 2 : ",end="")
print(equipe2)
