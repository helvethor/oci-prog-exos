##################################
# fichier le-juste-prix-validation.py
# nom de l'exercice :  Le juste prix
# url : http://www.france-ioi.org/algo/task.php?idChapter=647&idTask=0&sTab=task&iOrder=8
# type : validation
#
# Nom du chapitre : 
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

tours=int(input())-1
meilleur=int(input())
posmeilleur=1
pos=1

for loop in range(tours):
   pos=pos+1
   prix=int(input())
   if prix<=meilleur:
      posmeilleur=pos
      meilleur=prix
print(posmeilleur)
