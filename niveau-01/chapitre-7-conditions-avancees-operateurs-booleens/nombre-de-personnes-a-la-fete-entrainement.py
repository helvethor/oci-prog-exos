##################################
# fichier nombre-de-personnes-a-la-fete-entrainement.py
# nom de l'exercice :  Nombre de personnes à la fête
# url : http://www.france-ioi.org/algo/task.php?idChapter=648&idTask=0&sTab=task&iOrder=6
# type : entrainement
#
# Nom du chapitre : 
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nbmax=0
nbpers=0
for loop in range(2*int(input())):
   if int(input())>0:
      nbpers+=1
   else:
      nbpers-=1
   if nbpers>nbmax:
      nbmax=nbpers
print(nbmax)
