##################################
# fichier l-espion-est-demasque--entrainement.py
# nom de l'exercice :  L'espion est démasqué !
# url : http://www.france-ioi.org/algo/task.php?idChapter=648&idTask=0&sTab=task&iOrder=14
# type : entrainement
#
# Nom du chapitre : 
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

for loop in range(int(input())):
   prob=0
   taille=int(input())
   age=int(input())
   poids=int(input())
   cheval=int(input())
   brun=int(input())
   if taille>=178 and taille<=182:
      prob+=1
   if age>=34:
      prob+=1
   if poids<70:
      prob+=1
   if not cheval:
      prob+=1
   if brun:
      prob+=1
   if prob==5:
      print("Très probable")
   elif prob==3 or prob==4:
      print("Probable")
   elif prob==0:
      print("Impossible")
   else:
      print("Peu probable")
