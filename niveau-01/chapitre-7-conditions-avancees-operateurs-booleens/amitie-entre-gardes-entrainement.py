##################################
# fichier amitie-entre-gardes-entrainement.py
# nom de l'exercice :  Amitié entre gardes
# url : http://www.france-ioi.org/algo/task.php?idChapter=648&idTask=0&sTab=task&iOrder=5
# type : entrainement
#
# Nom du chapitre : 
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

d1=int(input())
f1=int(input())
d2=int(input())
f2=int(input())
if f2<d1 or f1<d2:
    print("Pas amis")
else:
    print("Amis")
