##################################
# fichier 04k-sans-espaces-obligatoire.py
# nom de l'exercice : Sans espaces
# url : http://www.france-ioi.org/algo/task.php?idChapter=595&idTask=0&sTab=task&iOrder=28
# type : obligatoire
#
# Chapitre : chapitre-3-chaines-de-caracteres
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

ch=input()
liCara=list(ch)
for idCara in range(len(liCara)):
   if liCara[idCara]==" ":
      liCara[idCara]="_"
ch="".join(liCara)
print(ch)
