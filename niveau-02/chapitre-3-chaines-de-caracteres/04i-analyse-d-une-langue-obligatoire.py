##################################
# fichier 04i-analyse-d-une-langue-obligatoire.py
# nom de l'exercice : Analyse d’une langue
# url : http://www.france-ioi.org/algo/task.php?idChapter=595&idTask=0&sTab=task&iOrder=26
# type : obligatoire
#
# Chapitre : chapitre-3-chaines-de-caracteres
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

lettreCle=input()
nbLignes=int(input())
nbAppa=0
for idLigne in range(nbLignes):
   ligne=input()
   for idCara in range(len(ligne)):
      if ligne[idCara]==lettreCle:
         nbAppa+=1
print(nbAppa)
