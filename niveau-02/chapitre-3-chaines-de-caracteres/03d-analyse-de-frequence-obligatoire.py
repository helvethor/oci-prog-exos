##################################
# fichier 03d-analyse-de-frequence-obligatoire.py
# nom de l'exercice : Analyse de fréquence
# url : http://www.france-ioi.org/algo/task.php?idChapter=595&idTask=0&sTab=task&iOrder=16
# type : obligatoire
#
# Chapitre : chapitre-3-chaines-de-caracteres
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nLignes,nMots=map(int,input().split(" "))
lNLettres=[0]*101
for numLigne in range(nLignes):
   mots=input().split(" ")
   for numMot in range(nMots):
      nLettres=len(mots[numMot])
      lNLettres[nLettres]+=1
for numNLettres in range(1,101):
   if lNLettres[numNLettres]!=0:
      print("{} : {}".format(numNLettres,lNLettres[numNLettres]))
