##################################
# fichier 04f-ngms-sns-vlls-obligatoire.py
# nom de l'exercice : ngms sns vlls
# url : http://www.france-ioi.org/algo/task.php?idChapter=595&idTask=0&sTab=task&iOrder=23
# type : obligatoire
#
# Chapitre : chapitre-3-chaines-de-caracteres
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

lLigne=[input(),input()]
for idLigne in range(2):
   ligneCon=""
   for idCara in range(len(lLigne[idLigne])):
      estConsonne=True
      for voyelle in ["A","E","I","O","U","Y"," "]:
         if lLigne[idLigne][idCara]==voyelle:
            estConsonne=False
      if estConsonne:
         ligneCon+=lLigne[idLigne][idCara]
   print(ligneCon)
