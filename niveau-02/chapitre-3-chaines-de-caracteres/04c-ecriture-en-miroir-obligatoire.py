##################################
# fichier 04c-ecriture-en-miroir-obligatoire.py
# nom de l'exercice : Écriture en miroir
# url : http://www.france-ioi.org/algo/task.php?idChapter=595&idTask=0&sTab=task&iOrder=20
# type : obligatoire
#
# Chapitre : chapitre-3-chaines-de-caracteres
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nLignes=int(input())
for idLigne in range(nLignes):
   ligne=input()
   for idCara in range(-1,-len(ligne)-1,-1):
      print(ligne[idCara], end="")
   print("")
