##################################
# fichier 04g-la-bataille-obligatoire.py
# nom de l'exercice : La bataille
# url : http://www.france-ioi.org/algo/task.php?idChapter=595&idTask=0&sTab=task&iOrder=24
# type : obligatoire
#
# Chapitre : chapitre-3-chaines-de-caracteres
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

ligne1=input()
ligne2=input()
idLettre=0
nEga=0
partieEnCours=True

if len(ligne1)<len(ligne2):
   lenMin=len(ligne1)
elif len(ligne1)>len(ligne2):
   lenMin=len(ligne2)
else:
   lenMin=len(ligne1)
   
while partieEnCours and idLettre<lenMin:
   if ligne1[idLettre]<ligne2[idLettre]:
      partieEnCours=False
      print(1)
   elif ligne1[idLettre]>ligne2[idLettre]:
      partieEnCours=False
      print(2)
   else:
      nEga+=1
   idLettre+=1
   
if ligne1==ligne2:
    print("=")
elif nEga==lenMin and len(ligne1)>len(ligne2):
    print(1)
elif nEga==lenMin and len(ligne1)<len(ligne2):
    print(2)
print(nEga)
