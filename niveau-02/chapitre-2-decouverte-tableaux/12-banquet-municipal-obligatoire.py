##################################
# fichier 12-banquet-municipal-obligatoire.py
# nom de l'exercice : Banquet municipal
# url : http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=17
# type : obligatoire
#
# Chapitre : chapitre-2-decouverte-tableaux
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nPos=int(input())
nChang=int(input())
lPos=[]
for numPos in range(nPos):
   lPos+=[int(input())]
lPosChang=lPos[:]
for numChang in range(nChang):
   num1=int(input())
   num2=int(input())
   lPosChang[num1]=lPos[num2]
   lPosChang[num2]=lPos[num1]
   lPos=lPosChang[:]
for numPos in range(nPos):
   print(lPosChang[numPos])
