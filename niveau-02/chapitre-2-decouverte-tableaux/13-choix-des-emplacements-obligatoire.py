##################################
# fichier 13-choix-des-emplacements-obligatoire.py
# nom de l'exercice : Choix des emplacements
# url : http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=18
# type : obligatoire
#
# Chapitre : chapitre-2-decouverte-tableaux
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nEmp=int(input())
lEmp=[]
for numEmp in range(nEmp):
   lEmp+=[int(input())]
lMar=lEmp[:]
for numEmp in range(nEmp):
   lMar[lEmp[numEmp]]=numEmp
for numEmp in range(nEmp):
   print(lMar[numEmp])
