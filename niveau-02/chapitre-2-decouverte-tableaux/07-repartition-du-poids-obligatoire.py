##################################
# fichier 07-repartition-du-poids-obligatoire.py
# nom de l'exercice : Répartition du poids
# url : http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=10
# type : obligatoire
#
# Chapitre : chapitre-2-decouverte-tableaux
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nChar=int(input())
lChar=[0]*nChar
pTot=0
for numChar in range(nChar):
   lChar[numChar]=float(input())
   pTot+=lChar[numChar]
pMoy=pTot/nChar
lDif=[0]*nChar
for numChar in range(nChar):
   lDif[numChar]=pMoy-lChar[numChar]
   print(lDif[numChar])
   
