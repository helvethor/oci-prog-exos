##################################
# fichier 08-visite-de-la-mine-obligatoire.py
# nom de l'exercice : Visite de la mine
# url : http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=11
# type : obligatoire
#
# Chapitre : chapitre-2-decouverte-tableaux
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nMv=int(input())
lMv=[]
lMvInv=[]
for numMv in range(nMv):
   lMv+=[int(input())]
   if lMv[numMv]==1:
      lMvInv+=[2]
   elif lMv[numMv]==2:
      lMvInv+=[1]
   elif lMv[numMv]==4:
      lMvInv+=[5]
   elif lMv[numMv]==5:
      lMvInv+=[4]
   else:
      lMvInv+=[3]
for numMv in range(nMv):
   print(lMvInv[nMv-1-numMv])
