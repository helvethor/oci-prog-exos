##################################
# fichier 06-etude-de-marche-obligatoire.py
# nom de l'exercice : Étude de marché
# url : http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=9
# type : obligatoire
#
# Chapitre : chapitre-2-decouverte-tableaux
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nProd=int(input())
lProd=[0]*nProd
nPers=int(input())
for numPers in range(nPers):
   lProd[int(input())]+=1
for loop in range(nProd):
   print(lProd[loop])
