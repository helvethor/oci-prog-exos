##################################
# fichier 06-formes-creuses-obligatoire.py
# nom de l'exercice : Formes creuses
# url : http://www.france-ioi.org/algo/task.php?idChapter=509&idTask=0&sTab=task&iOrder=13
# type : obligatoire
#
# Chapitre : chapitre-4-fonctions
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

def affiLigne(nbCara):
    for idCara in range(nbCara):
        print("X",end="")
    print("")
        
def affiRect(nbLignes,nbCols):
    for idCol in range(nbCols):
        print("#",end="")
    print("")
    for idLignes in range(1,nbLignes-1):
        if nbCols>1:
            print("#",end="")
        for idCol in range(1,nbCols-1):
            print(" ",end="")    
        print("#")
    if nbLignes>1:
        for idCol in range(nbCols):
            print("#",end="")
        print("")

def affiTri(nbLignes):
    print("@")
    if nbLignes>1:
        print("@@")
    for idLigne in range(2,nbLignes-1):
        print("@",end="")
        for idEsp in range(1,idLigne):
            print(" ",end="")
        print("@")
    if nbLignes>2:
        for idLigne in range(nbLignes):
            print("@",end="")
        print("")

nbCara=int(input())
nbLignes=int(input())
nbCols=int(input())
coteTri=int(input())
        
affiLigne(nbCara)
affiRect(nbLignes,nbCols)
affiTri(coteTri)
