##################################
# fichier 07-convertisseur-d-unites-obligatoire.py
# nom de l'exercice : Convertisseur d'unités
# url : http://www.france-ioi.org/algo/task.php?idChapter=509&idTask=0&sTab=task&iOrder=14
# type : obligatoire
#
# Chapitre : chapitre-4-fonctions
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

nbLignes=int(input())
lQtt=[0]*nbLignes
lUnite=[0]*nbLignes
for idLigne in range(nbLignes):
    lQtt[idLigne],lUnite[idLigne]=input().split(" ")
    lQtt[idLigne]=float(lQtt[idLigne])
for idLigne in range(nbLignes):
    if lUnite[idLigne]=="m":
        print("{} p".format(lQtt[idLigne]/0.3048))
    elif lUnite[idLigne]=="g":
        print("{} l".format(lQtt[idLigne]*0.002205))
    elif lUnite[idLigne]=="c":
        print("{} f".format(lQtt[idLigne]*1.8+32))
