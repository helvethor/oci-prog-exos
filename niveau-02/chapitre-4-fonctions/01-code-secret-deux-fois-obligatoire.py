##################################
# fichier 01-code-secret-deux-fois-obligatoire.py
# nom de l'exercice : Code secret deux fois
# url : http://www.france-ioi.org/algo/task.php?idChapter=509&idTask=0&sTab=task&iOrder=1
# type : obligatoire
#
# Chapitre : chapitre-4-fonctions
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

def testCode():
   codeSecret=4242
   codeVrai=False
   while not codeVrai:
      print("Entrez le code :")
      if int(input())==codeSecret:
         codeVrai=True
   return
   
def main():
   testCode()
   print("Encore une fois.")
   testCode()
   print("Bravo.")

main()
