##################################
# fichier 02-deux-codes-secrets-obligatoire.py
# nom de l'exercice : Deux codes secrets
# url : http://www.france-ioi.org/algo/task.php?idChapter=509&idTask=0&sTab=task&iOrder=3
# type : obligatoire
#
# Chapitre : chapitre-4-fonctions
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

def testCode(codeSecret):
   tentative=codeSecret+1
   while tentative!=codeSecret:
      print("Entrez le code :")
      tentative=int(input())
testCode(4242)
print("Premier code bon.")
testCode(2121)
print("Bravo.")
