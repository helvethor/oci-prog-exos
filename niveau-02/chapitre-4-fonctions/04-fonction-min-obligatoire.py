##################################
# fichier 04-fonction-min-obligatoire.py
# nom de l'exercice : Fonction min
# url : http://www.france-ioi.org/algo/task.php?idChapter=509&idTask=0&sTab=task&iOrder=7
# type : obligatoire
#
# Chapitre : chapitre-4-fonctions
#
# Compétence développée : 
#
# auteur : 
##################################

# chargement des modules


# mettre votre code ici

def min(nb1,nb2):
    if nb1<=nb2:
        return nb1
    else:
        return nb2
liNb=[]
for idNb in range(10):
    liNb+=[int(input())]
nbMin=liNb[0]
for idNb in range(1,10):
    nbMin=min(nbMin,liNb[idNb])
print(nbMin)
